#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/bootdevice/by-name/recovery:13980576:6a159de7d8c2afe4948757ce29b80f2c43e6a235; then
  applypatch  EMMC:/dev/block/platform/bootdevice/by-name/boot:8747936:e1311300c154b6b484a0e3bf5ec082e717b5990c EMMC:/dev/block/platform/bootdevice/by-name/recovery 6a159de7d8c2afe4948757ce29b80f2c43e6a235 13980576 e1311300c154b6b484a0e3bf5ec082e717b5990c:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi

#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_ELUGA_I7_EE.mk

COMMON_LUNCH_CHOICES := \
    lineage_ELUGA_I7_EE-user \
    lineage_ELUGA_I7_EE-userdebug \
    lineage_ELUGA_I7_EE-eng

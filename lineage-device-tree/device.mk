#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Health
PRODUCT_PACKAGES += \
    android.hardware.health@2.1-impl \
    android.hardware.health@2.1-impl.recovery \
    android.hardware.health@2.1-service

# Overlays
PRODUCT_ENFORCE_RRO_TARGETS := *

# Product characteristics
PRODUCT_CHARACTERISTICS := default

# Rootdir
PRODUCT_PACKAGES += \

PRODUCT_PACKAGES += \
    fstab.enableswap \
    init.mt6739.rc \
    meta_init.connectivity.rc \
    factory_init.connectivity.rc \
    init.mt6739.usb.rc \
    init.project.rc \
    meta_init.rc \
    meta_init.project.rc \
    meta_init.modem.rc \
    multi_init.rc \
    init.modem.rc \
    init.aee.rc \
    init.sensor_1_0.rc \
    init.ago.rc \
    factory_init.rc \
    init.connectivity.rc \
    factory_init.project.rc \
    init.rc \
    ueventd.rc \
    init.recovery.mt6739.rc \

# Shipping API level
PRODUCT_SHIPPING_API_LEVEL := 28

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

# Inherit the proprietary files
$(call inherit-product, vendor/panasonic/ELUGA_I7_EE/ELUGA_I7_EE-vendor.mk)
